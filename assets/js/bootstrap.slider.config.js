const carousel = document.getElementById("homeSlider");
if (carousel) {
  const items = carousel.querySelectorAll(".carousel-item");
  // set slider interval
  var intVal = 6000;
  const bootstrapCarousel = new bootstrap.Carousel(carousel, {
    interval: intVal,
    pause: false,
  });

  // Function to hide elements in a given item
  function hideElements(item) {
    /***********************
     * Remove Old Animations
     * *********************/

    item
      .querySelector("h2")
      .classList.remove("animate__animated", "animate__fadeInDown");
    item
      .querySelector("p")
      .classList.remove("animate__animated", "animate__fadeInDown");
    item
      .querySelector(".btn-wrap")
      .classList.remove("animate__animated", "animate__fadeInUp");

    /***********************
     *    New Animations
     * *********************/
    item
      .querySelector("h2")
      .classList.add("animate__animated", "animate__fadeOutUp");
    item
      .querySelector("p")
      .classList.add("animate__animated", "animate__fadeOut");
    item
      .querySelector(".btn-wrap")
      .classList.add("animate__animated", "animate__fadeOutDown");
  }
  // Function to hide elements in a given item
  function showElements(item) {
    /***********************
     * Remove Old Animations
     * *********************/
    item
      .querySelector("h2")
      .classList.remove("animate__animated", "animate__fadeOutUp");
    item
      .querySelector("p")
      .classList.remove("animate__animated", "animate__fadeOut");
    item
      .querySelector(".btn-wrap")
      .classList.remove("animate__animated", "animate__fadeOutDown");
    /***********************
     *    New Animations
     * *********************/
    item
      .querySelector("h2")
      .classList.add("animate__animated", "animate__fadeInDown");
    item
      .querySelector("p")
      .classList.add("animate__animated", "animate__fadeInDown");
    item
      .querySelector(".btn-wrap")
      .classList.add("animate__animated", "animate__fadeInUp");
  }

  // Function to handle slide change
  function handleSlideChange() {
    items.forEach((item) => {
      if (item.classList.contains("active")) {
        // Set Image Transition scale
        item.querySelector("img").classList.add("scale_up");
        showElements(item);
        var intval = intVal - 1200;
        setTimeout(() => {
          hideElements(item);
        }, intval);
      } else {
        // Remove Image Transition scale
        item.querySelector("img").classList.remove("scale_up");
        hideElements(item);
      }
    });
  }

  // Attach event listener to the carousel
  carousel.addEventListener("slid.bs.carousel", handleSlideChange);

  // Initial execution to handle the current active slide
  handleSlideChange();
}
